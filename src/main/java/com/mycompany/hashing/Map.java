/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashing;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author user
 */
public class Map<K, V> {

    private ArrayList<HashNode<K, V>> tableArray;
    private int size;
    private int numTable;

    public Map() {
        tableArray = new ArrayList<>();
        numTable = 10;
        size = 0;

        //Create empty chains
        for (int i = 0; i < numTable; i++) {
            tableArray.add(null);
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private final int hashCode(K key) {
        return Objects.hashCode(key);
    }

    //hash function to find index
    private int getTableInex(K key) {
        int hashCode = hashCode(key);
        int index = hashCode % numTable;

        index = index < 0 ? index * -1 : index;
        return index;
    }

    //remove a given key
    public V remove(K key) {
        int tableIndex = getTableInex(key);
        int hashCode = hashCode(key);

        HashNode<K, V> head = tableArray.get(tableIndex);

        //Search for key in its chain
        HashNode<K, V> prev = null;
        while (head != null) {
            if (head.key.equals(key) && hashCode == head.hashCode) {
                break;
            }

            prev = head;
            head = head.next;
        }

        if (head == null) {
            return null;
        }

        size--;

        if (prev != null) {
            prev.next = head.next;
        } else {
            tableArray.set(tableIndex, head.next);
        }

        return head.value;
    }

    // Returns value for a key
    public V get(K key) {

        int bucketIndex = getTableInex(key);
        int hashCode = hashCode(key);

        HashNode<K, V> head = tableArray.get(bucketIndex);

        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode) {
                return head.value;
            }
            head = head.next;
        }

        return null;
    }

    // Adds a key value pair to hash
    public void add(K key, V value) {
        // Find head of chain for given key
        int bucketIndex = getTableInex(key);
        int hashCode = hashCode(key);
        HashNode<K, V> head = tableArray.get(bucketIndex);

        // Check if key is already present
        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode) {
                head.value = value;
                return;
            }
            head = head.next;
        }

        // Insert key in chain
        size++;
        head = tableArray.get(bucketIndex);
        HashNode<K, V> newNode
                = new HashNode<K, V>(key, value, hashCode);
        newNode.next = head;
        tableArray.set(bucketIndex, newNode);

        // If load factor goes beyond threshold, then
        // double hash table size
        if ((1.0 * size) / numTable >= 0.7) {
            ArrayList<HashNode<K, V>> temp = tableArray;
            tableArray = new ArrayList<>();
            numTable = 2 * numTable;
            size = 0;
            for (int i = 0; i < numTable; i++) {
                tableArray.add(null);
            }

            for (HashNode<K, V> headNode : temp) {
                while (headNode != null) {
                    add(headNode.key, headNode.value);
                    headNode = headNode.next;
                }
            }
        }
    }

}
